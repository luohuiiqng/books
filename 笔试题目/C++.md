# 淘宝笔试题目

题目大意如下：请使用C语言完成strnicmp的编码实现，要求不能调用任何其他函数。strcicmp完成两个ascii字符串的比较，忽略大小写（两个英文字母比时，认为大小写无差别），最多比较n个字符（当两个字符串长度超过n时，就认为它们的长度都等于n），返回<0表示第一个字符串小于第二个字符串，返回>0表示第一个字符串大于第二个字符串，返回等于0表示两个字符串相等。函数声明如下：int strnicmp(char  const*s1,char const *s2,int n)

答：

```C++

#include <QCoreApplication>
#include<stdio.h>

#define MAX_SIZE 1024


int stricmp(char const*s1,char const*s2,int n)
{
     char const *t1=s1;
     char const *t2=s2;


//    while(*t3!='\0')
//    {
//        printf("%c",*t3);
//        t3++;
//    }


    int n1=0,n2=0,re=999;
    while(*t1++!='\0')
    {
        n1++;
    }

    while(*t2++!='\0')
    {
        n2++;
    }

    if(n1>n&&n2>n)
    {
        re= 0;
    }
    else if(n1>n2)
    {
        re= 1;
    }
    else if(n1<=n2)
    {
        re= -1;
    }

    return re;
}

int con_cin(char *s,int len)
{
    int i=0,err=0;
    if(NULL==s)
        return -1;
    char ch='\0';
    while((ch=getchar())!='\n')
    {
        if(i<(len-1))
        {
            s[i]=ch;
            i++;
        }
        else
        {
            err=1;
        }
    }
    s[i]='\0';
    return err;
}

int main(int argc, char *argv[])
{
    //QCoreApplication a(argc, argv);

    int enter_err=-1;
    while(1)
    {
        char enter[MAX_SIZE]={'\0'};
        enter_err=con_cin(enter,MAX_SIZE);
        if(enter_err==0)
        {
            if(strlen(enter)>0)
            {
                printf("输入的内容是:%s\n",enter);
            }
        }
        else
        {
            if(enter_err>0)
            {
                printf("你输入的内容太多!\n");
            }
            else
            {
                printf("未知错误!");
                break;
            }
        }
    }

    return 0;
}

```

